import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./auth/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./auth/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'lupa-password',
    loadChildren: () => import('./auth/lupa-password/lupa-password.module').then(m => m.LupaPasswordPageModule)
  },

  {
    path: 'guru',
    loadChildren: () => import('./guru/guru.module').then(m => m.GuruPageModule)
  },
  {
    path: 'siswa',
    loadChildren: () => import('./Siswa/siswa.module').then(m => m.SiswaPageModule)
  },









];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
