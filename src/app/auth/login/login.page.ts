import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { ModalController, ToastController } from '@ionic/angular';
interface User {
  name: string;
  address: string;
  image: Object;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user: any = {};
  userData: User;

  constructor(
    public modalController: ModalController,
    public toastController: ToastController,
    public auth: AngularFireAuth,
    public db: AngularFirestore,
    public router: Router,

  ) { }

  ngOnInit() {
  }
  async pesangagal() {
    const toast = await this.toastController.create({
      message: 'Kesalahan saat login, silahkan ulangi lagi',
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
  login() {

    this.auth.signInWithEmailAndPassword(this.user.email, this.user.password).then(res => {
      if (res.user) {
        this.cekPeran(res.user.email);
      }
    }).catch(err => {
      this.pesangagal();


    });
  }

  async cekPeran(email) {
    this.db.collection('users').doc(email).get().subscribe(res => {
      this.routerControl(res.data());
    })

  }

  routerControl(data) {
    if (data.peran == 'siswa') this.router.navigate(['siswa']);
    if (data.peran == 'guru') this.router.navigate(['guru']);
  }








}
