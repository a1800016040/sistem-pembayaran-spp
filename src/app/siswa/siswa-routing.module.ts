import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SiswaPage } from './siswa.page';

const routes: Routes = [
  {
    path: '',
    component: SiswaPage,
    children: [
      {
        path: '',
        redirectTo: 'beranda',
        pathMatch: 'full'
      },
      {
        path: 'beranda',
        loadChildren: () => import('../Siswa/beranda/beranda.module').then(m => m.BerandaPageModule)
      },
      {
        path: 'tagihan',
        loadChildren: () => import('../Siswa/tagihan/tagihan.module').then(m => m.TagihanPageModule)
      },
      {
        path: 'barcode',
        loadChildren: () => import('../Siswa/barcode/barcode.module').then(m => m.BarcodePageModule)
      },

      {
        path: 'pemberitahuan',
        loadChildren: () => import('./pemberitahuan/pemberitahuan.module').then(m => m.PemberitahuanPageModule)
      },
      {
        path: 'akun',
        loadChildren: () => import('./akun/akun.module').then(m => m.AkunPageModule)
      },
      {
        path: 'riwayat-pembayaran',
        loadChildren: () => import('./akun/riwayat-pembayaran/riwayat-pembayaran.module').then(m => m.RiwayatPembayaranPageModule)
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SiswaPageRoutingModule { }
