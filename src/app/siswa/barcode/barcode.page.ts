import { Component, OnInit } from '@angular/core';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.page.html',
  styleUrls: ['./barcode.page.scss'],
})
export class BarcodePage implements OnInit {
  qrData = 'ilham zahidin, 500.000';
  createdCode = null;
  scannedCode = null;
  constructor(private barcodeScanner: BarcodeScanner) {
  }

  ngOnInit() {
  }
  createCode() {
    this.createdCode = this.qrData;
    console.log(this.createdCode);
  }

  scanCode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
    })
  }
  segmentChanged(ev: any) {
    console.log('Segment changed', ev);
  }
}

