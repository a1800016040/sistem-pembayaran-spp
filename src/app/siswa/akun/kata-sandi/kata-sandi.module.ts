import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { KataSandiPageRoutingModule } from './kata-sandi-routing.module';

import { KataSandiPage } from './kata-sandi.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    KataSandiPageRoutingModule
  ],
  declarations: [KataSandiPage]
})
export class KataSandiPageModule {}
