import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { KataSandiPage } from './kata-sandi.page';

const routes: Routes = [
  {
    path: '',
    component: KataSandiPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class KataSandiPageRoutingModule {}
