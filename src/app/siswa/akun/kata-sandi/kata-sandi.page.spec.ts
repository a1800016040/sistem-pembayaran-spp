import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { KataSandiPage } from './kata-sandi.page';

describe('KataSandiPage', () => {
  let component: KataSandiPage;
  let fixture: ComponentFixture<KataSandiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KataSandiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(KataSandiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
