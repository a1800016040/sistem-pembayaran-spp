import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BiodataPage } from './biodata.page';

describe('BiodataPage', () => {
  let component: BiodataPage;
  let fixture: ComponentFixture<BiodataPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiodataPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BiodataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
