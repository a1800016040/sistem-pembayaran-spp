import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AkunPage } from './akun.page';

const routes: Routes = [
  {
    path: '',
    component: AkunPage
  },
  {
    path: 'biodata',
    loadChildren: () => import('./biodata/biodata.module').then( m => m.BiodataPageModule)
  },
  {
    path: 'kata-sandi',
    loadChildren: () => import('./kata-sandi/kata-sandi.module').then( m => m.KataSandiPageModule)
  },
  {
    path: 'riwayat-pembayaran',
    loadChildren: () => import('./riwayat-pembayaran/riwayat-pembayaran.module').then( m => m.RiwayatPembayaranPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AkunPageRoutingModule {}
