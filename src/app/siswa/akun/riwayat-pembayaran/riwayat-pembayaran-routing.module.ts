import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RiwayatPembayaranPage } from './riwayat-pembayaran.page';

const routes: Routes = [
  {
    path: '',
    component: RiwayatPembayaranPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RiwayatPembayaranPageRoutingModule {}
