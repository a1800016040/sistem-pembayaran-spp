import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-siswa',
  templateUrl: './siswa.page.html',
  styleUrls: ['./siswa.page.scss'],
})
export class SiswaPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  tagihan() {
    this.router.navigate(['/tagihan'])
  }

}
