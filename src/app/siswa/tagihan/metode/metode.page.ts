
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-metode',
  templateUrl: './metode.page.html',
  styleUrls: ['./metode.page.scss'],
})
export class MetodePage implements OnInit {

  constructor(public modalcontroller: ModalController) { }

  ngOnInit() {
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalcontroller.dismiss({
      'dismissed': true
    });
  }
}
