import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-pembayaran',
  templateUrl: './info-pembayaran.page.html',
  styleUrls: ['./info-pembayaran.page.scss'],
})
export class InfoPembayaranPage implements OnInit {

  constructor(public modalcontroller: ModalController) { }

  ngOnInit() {
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalcontroller.dismiss({
      'dismissed': true
    });
  }

}
