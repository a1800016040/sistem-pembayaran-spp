import { InfoPembayaranPage } from './info-pembayaran/info-pembayaran.page';
import { MetodePage } from './metode/metode.page';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-tagihan',
  templateUrl: './tagihan.page.html',
  styleUrls: ['./tagihan.page.scss'],
})
export class TagihanPage implements OnInit {

  constructor(public modalc: ModalController) { }

  ngOnInit() {
  }

  async metode() {
    const modal = await this.modalc.create({
      component: MetodePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
  async bayar() {
    const modal = await this.modalc.create({
      component: InfoPembayaranPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}
