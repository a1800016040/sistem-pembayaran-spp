import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TagihanPage } from './tagihan.page';

const routes: Routes = [
  {
    path: '',
    component: TagihanPage
  },
  {
    path: 'metode',
    loadChildren: () => import('./metode/metode.module').then(m => m.MetodePageModule)
  },
  {
    path: 'info-pembayaran',
    loadChildren: () => import('./info-pembayaran/info-pembayaran.module').then( m => m.InfoPembayaranPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TagihanPageRoutingModule { }
