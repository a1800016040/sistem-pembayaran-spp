import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailPemberitahuanPageRoutingModule } from './detail-pemberitahuan-routing.module';

import { DetailPemberitahuanPage } from './detail-pemberitahuan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailPemberitahuanPageRoutingModule
  ],
  declarations: [DetailPemberitahuanPage]
})
export class DetailPemberitahuanPageModule {}
