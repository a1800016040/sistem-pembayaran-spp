import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailPemberitahuanPage } from './detail-pemberitahuan.page';

describe('DetailPemberitahuanPage', () => {
  let component: DetailPemberitahuanPage;
  let fixture: ComponentFixture<DetailPemberitahuanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPemberitahuanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailPemberitahuanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
