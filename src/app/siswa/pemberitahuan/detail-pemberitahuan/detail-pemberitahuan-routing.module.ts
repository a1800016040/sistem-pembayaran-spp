import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailPemberitahuanPage } from './detail-pemberitahuan.page';

const routes: Routes = [
  {
    path: '',
    component: DetailPemberitahuanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailPemberitahuanPageRoutingModule {}
