import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PemberitahuanPage } from './pemberitahuan.page';

const routes: Routes = [
  {
    path: '',
    component: PemberitahuanPage
  },
  {
    path: 'detail-pemberitahuan',
    loadChildren: () => import('./detail-pemberitahuan/detail-pemberitahuan.module').then( m => m.DetailPemberitahuanPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PemberitahuanPageRoutingModule {}
