import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PemberitahuanPageRoutingModule } from './pemberitahuan-routing.module';

import { PemberitahuanPage } from './pemberitahuan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PemberitahuanPageRoutingModule
  ],
  declarations: [PemberitahuanPage]
})
export class PemberitahuanPageModule {}
