import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PemberitahuanPage } from './pemberitahuan.page';

describe('PemberitahuanPage', () => {
  let component: PemberitahuanPage;
  let fixture: ComponentFixture<PemberitahuanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PemberitahuanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PemberitahuanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
