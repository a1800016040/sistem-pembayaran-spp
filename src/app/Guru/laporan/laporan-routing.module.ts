import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LaporanPage } from './laporan.page';

const routes: Routes = [
  {
    path: '',
    component: LaporanPage,
  },
  {
    path: 'detail',
    loadChildren: () => import('../../Guru/laporan/detail/detail.module').then( m => m.DetailPageModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LaporanPageRoutingModule {}
