import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuruPage } from './guru.page';

const routes: Routes = [
  {
    path: '',
    component: GuruPage,
    children: [
      {
          path: '',
          redirectTo: 'beranda',
          pathMatch: 'full'
      },
      {
        path: 'beranda',
        loadChildren: () => import('../Guru/beranda/beranda.module').then( m => m.BerandaPageModule)
      },
      {
        path: 'laporan',
        loadChildren: () => import('../Guru/laporan/laporan.module').then( m => m.LaporanPageModule)
      },
    ]
  },
  

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GuruPageRoutingModule {}
