// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAiO8vxWfdHvHSmR0V_LNuLrU0TTUEFdS4",
    authDomain: "simple-app-2020.firebaseapp.com",
    projectId: "simple-app-2020",
    storageBucket: "simple-app-2020.appspot.com",
    messagingSenderId: "115838502527",
    appId: "1:115838502527:web:46754a81fc5a4e849ca14a",
    measurementId: "G-W10W50GQDZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
